import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormAction } from "../store/BTForm/slice";

const TableProduct = () => {
  const { productList } = useSelector((state) => state.btForm);
  const dispatch = useDispatch();

  const [searchValue, setSearchValue] = useState("");
  const [searchResults, setSearchResults] = useState();
  const handleChange = (event) => {
    setSearchValue(event.target.value);
    const result = productList.filter((prd) =>
      prd.hoTen.trim().toLowerCase().includes(searchValue.toLowerCase())
    );
    setSearchResults(result);
  };
  const searchList = searchValue ? searchResults : productList;

  return (
    <div className="mt-3 container">
        <div className=" form-group  row d-flex pl-0 mr-0">
            <div className="col-10">
                <input
                className="form-control"
                type="search"
                placeholder="Nhập tên cần tìm..."
                value={searchValue}
                onChange={handleChange}

                
                />
            </div>
            <button className="col-2 btn btn-success 
            "
            onClick={handleChange}
                
                
               
            >Tìm kiếm</button>
        </div>

      <table className="table">
        <thead className="bg-dark text-white">
          <tr>
            <th>Mã SV</th>
            <th>Họ Tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Tools</th>
          </tr>
        </thead>
        <tbody>
          {searchList?.map((prd) => (
            <tr key={prd?.id}>
              <td>{prd.id}</td>
              <td>{prd.hoTen}</td>
              <td>{prd.sdt}</td>
              <td>{prd.email}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => {
                    dispatch(btFormAction.deleteProduct(prd.id));
                  }}
                >
                  Delete
                </button>
                <button
                  className="btn btn-info ml-3"
                  onClick={() => {
                    dispatch(btFormAction.editProduct(prd));
                  }}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableProduct;
