import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormAction } from "../store/BTForm/slice";

const FormProduct = () => {
  const [formData, setFormData] = useState();

  const [formError, setFormError] = useState();

  const dispatch = useDispatch();

  const { productEdit } = useSelector((state) => state.btForm);

  const handleFormData = () => (ev) => {
    const {name, value,title,minLength, max,validity} = ev.target;
    let mess;
    if(minLength !== -1 && !value.length) {
        mess = `Vui lòng nhập thông tin ${title}`
    }else if(value.length < minLength) {
        mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`
    }else if(value.length > max && max) {
        mess = `Chỉ được nhập tối đa ${max} ký tự `
    }else if(validity.patternMismatch && ["id","sdt"].includes(name)){
        mess = `Vui lòng nhập ký tự là số`
    }else if(validity.patternMismatch && ["email"].includes(name)){
        mess = `Vui lòng nhập đúng email `
    }
    setFormError({
      ...formError,
      [name]:  mess,
    });
  
    setFormData({
      ...formData,
      [name]: mess ? undefined : value
    })
  };


  useEffect(() => {
    if (!productEdit) return;
    setFormData(productEdit);
  }, [productEdit]);

  return (
    <form className=" container"
    onSubmit={(event) => {
        
        event.preventDefault();

        const elements = document.querySelectorAll('input')
        let formError = {}
        elements.forEach((ele)=> {
            let mess;
            const {name, value,title,minLength, max,validity} = ele;
            if(minLength !== -1 && !value.length) {
                mess = `Vui lòng nhập thông tin ${title}`
            }else if(value.length < minLength) {
                mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`
            }else if(value.length > max && max) {
                mess = `Chỉ được nhập tối đa ${max} ký tự `
            }else if(validity.patternMismatch && ["id","sdt"].includes(name)){
                mess = `Vui lòng nhập ký tự là số`
            }else if(validity.patternMismatch && ["email"].includes(name)){
                mess = `Vui lòng nhập đúng email `
            }
            formError[name] = mess
            
        })
        let flag = false

        for(let key in formError) {
            if(formError[key]) {
                flag = true;
                break;
            }
        }
        if(flag) {
            setFormError(formError)
            return
        }
        if(productEdit) {
            dispatch(btFormAction.updateProduct(formData))
        }else {
            dispatch(btFormAction.addProduct(formData))
        }
        
    }}
    noValidate>
      <h2 className="px-2 py-4 bg-dark text-white text-center">
        Thông tin sinh viên
      </h2>
      <div className="form-group row">
        <div className="col-6">
          <p className="font-weight-bold">Mã SV</p>
          <input
            value={formData?.id}
            type="text"
            className="form-control"
            name="id"
            title="id"
            minLength={1}
            max={5}
            pattern="^[0-9]+$"
            disabled={!!productEdit}
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.id}</p>
        </div>
        <div className="col-6">
          <p className="font-weight-bold">Họ tên </p>
          <input
            value={formData?.hoTen}
            type="text"
            className="form-control"
            name="hoTen"
            title="Họ tên"
            minLength={1}
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.hoTen}</p>
        </div>
      </div>
      <div className="form-group row">
        <div className="col-6">
          <p className="font-weight-bold">Số điện thoại</p>
          <input
            value={formData?.sdt}
            type="text"
            className="form-control"
            name="sdt"
            title="Số điện thoại"
            minLength={10}
            pattern="^[0-9]+$"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.sdt}</p>
        </div>
        <div className="col-6">
          <p className="font-weight-bold">Email</p>
          <input
            value={formData?.email}
            type="text"
            className="form-control"
            name="email"
            title="email"
            minLength={1}
            pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.email}</p>
        </div>
      </div>
      <div className="mt-3">
        {productEdit && <button className="btn btn-info ">Update</button>}
        {!productEdit && (
          <button className="btn btn-success ">Thêm sinh viên </button>
        )}
      </div>
    </form>
  );
};

export default FormProduct;
