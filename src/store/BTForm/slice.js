import { createSlice } from "@reduxjs/toolkit"


const initialState = {
    productList: [],
    productEdit: undefined
}

const btFormSlice = createSlice({
    name: 'btForm',
    initialState,
    reducers: {
        addProduct: (state, action) => {
            state.productList.push(action.payload)
        },
        deleteProduct: (state, action) => {
            state.productList = state.productList.filter((prd) => prd.id !== action.payload)
        },
        editProduct: (state, action) => {
            state.productEdit = action.payload
        },
        updateProduct: (state, action ) => {
            state.productList = state.productList.map((prd) => {
                if(prd.id === action.payload.id) {
                    return action.payload
                }
                return prd
            })
            state.productEdit = undefined
        }
    }
})


export const {actions: btFormAction , reducer: btFormReducer} = btFormSlice